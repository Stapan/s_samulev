public class Task10 {
    public static void main(String[] args) {

        int smallest = 0;

        for (int num = 1; num < 1000000000; num += 2) {
            boolean isRop = true;


            for (int rop = 0; rop < 20; rop++) {

                if (num % rop != 0) {
                    isRop = false;
                }

                if (isRop == true) {
                    smallest = num;
                    break;
                }
            }
            System.out.println(smallest);
        }
    }
}
