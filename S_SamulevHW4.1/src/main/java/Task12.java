import java.util.Scanner;

public class Task12 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите дистанцию первого дня:");
        double firstDayDistance = scanner.nextDouble();

        System.out.println("Введите количество километров, которые должен пробежать спортсмен:");
        double allDistance = scanner.nextDouble();

        int day = 0;

        do {
            firstDayDistance =+ firstDayDistance * 1.1;
            day++;
        }
        while (firstDayDistance <= allDistance);
        System.out.println(day);
    }
}
