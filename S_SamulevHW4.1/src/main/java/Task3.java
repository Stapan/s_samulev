public class Task3 {
    public static void main(String[] args) {
        for (int i = 10; i <= 1000000; i++) {


            int firstDigit = (i / 1000000);
            int secondDigit = (i % 1000000) / 100000;
            int thirdDigit = (i % 10000) / 1000;
            int fourthDigit = (i % 1000) / 100;
            int fivethDigit = (i % 100) / 10;
            int sixthDigit = (i % 10);

            if ((firstDigit * firstDigit * firstDigit)
                    + (secondDigit * secondDigit * secondDigit)
                    + (thirdDigit * thirdDigit * thirdDigit)
                    + (fourthDigit * fourthDigit * fourthDigit)
                    + (fivethDigit * fivethDigit * fivethDigit)
                    + (sixthDigit * sixthDigit * sixthDigit) == i) {

                System.out.println("This is a armstriong number : " + i);

            }

        }
    }
}
