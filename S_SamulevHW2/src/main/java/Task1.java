import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите число:");
        int x = scanner.nextInt();

        System.out.print("В двоичном виде: 0");
        System.out.println(Integer.toBinaryString(x));
    }
}
