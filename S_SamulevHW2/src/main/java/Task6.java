import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите число:");
        int x = scanner.nextInt();

        System.out.println("!!!Важный момент!!! В программировании отсчёт начинается с 0!!!");

        System.out.println("Произойдёт обнуление всех битов кроме последних i битов вашего числа. Введите i:");
        int i = scanner.nextByte();

        x &= ~(1 << i) - 1;

        System.out.print("Вывод:");
        System.out.println(Integer.toBinaryString(x));
    }
}
