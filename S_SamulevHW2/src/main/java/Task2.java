
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int first = 2;

        System.out.println("Введите число, сколько раз вы хотите возвести двойку в сепень:");
        int second = scanner.nextInt();

        int result = (first << second);
        System.out.println("В двоичном коде:");
        System.out.print(Integer.toBinaryString(result));

        System.out.println("Обычный код:");
        System.out.print(result);
    }
}
