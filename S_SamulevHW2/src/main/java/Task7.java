import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите число:");
        int x = scanner.nextInt();

        System.out.println("!!!Важный момент!!! В программировании отсчёт начинается с 0!!!");

        System.out.println("Введите число, номер бита которого вы хотите узнать:");
        int i = scanner.nextByte();

        int IbitValue = (x << i) & 1;

        System.out.print("Вывод:");
        System.out.println(Integer.toBinaryString(IbitValue));
    }
}
