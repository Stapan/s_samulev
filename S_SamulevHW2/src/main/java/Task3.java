import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner =  new Scanner(System.in);

        System.out.println("Введите число:");
        int x = scanner.nextInt();

        System.out.println("!!!Важный момент!!! В программировании отсчёт начинается с 0!!!");

        System.out.println("Введите любое число чтобы установить там бит равный 1:");
        int i = scanner.nextByte();

        x |= (1 << i);

        System.out.print("Вывод:");
        System.out.println(Integer.toBinaryString(x));

    }
}
